﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace FunctionExtensions
{
    public static class FuncExtensions
    {
         public static Func<T, TResult> Memoize<T, TResult>(this Func<T, TResult> f)
         {
             Contract.Requires(f != null);
             Contract.Ensures(Contract.Result<Func<T, TResult>>() != null);

             var cache = new Dictionary<T, TResult>();
             var isNullArgumentCached = false;
             var nullArgumentResult = default(TResult);
             return argument =>
                 {
                     if (argument as object == null)
                     {
                         if (!isNullArgumentCached)
                         {
                             nullArgumentResult = f(argument);
                             isNullArgumentCached = true;
                         }
                         return nullArgumentResult;
                     }

                     TResult result;
                     if (!cache.TryGetValue(argument, out result))
                     {
                         result = f(argument);
                         cache.Add(argument, result);
                     }
                     return result;
                 };
         }
    }
}