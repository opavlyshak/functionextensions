using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using NUnit.Framework;

namespace FunctionExtensions
{
    [PexClass(typeof(FuncExtensions))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestFixture]
    public partial class FuncExtensionsTests
    {
        [PexGenericArguments(typeof(int), typeof(int))]
        [PexMethod]
        public Func<T, TResult> Memoize_SmokeTest<T,TResult>(Func<T, TResult> f)
        {
            Func<T, TResult> result = f.Memoize();
            return result;
        }

        [PexGenericArguments(typeof(int), typeof(int))]
        [PexMethod]
        public Func<T, TResult> MemoizedFunctionWithValueTypeInput_SmokeTest<T,TResult>(
            Func<T, TResult> f,
            T inputParameter)
        {
            var memoizedFunc = f.Memoize();
            var result = memoizedFunc(inputParameter);
            PexObserve.Value("result", result);
            return memoizedFunc;
        }

        [PexGenericArguments(typeof(object), typeof(int))]
        [PexMethod]
        public Func<T, TResult> MemoizedFunctionWithReferenceTypeInput_SmokeTest<T,TResult>(
            Func<T, TResult> f,
            T inputParameter)
        {
            var memoizedFunc = f.Memoize();
            var result = memoizedFunc(inputParameter);
            PexObserve.Value("result", result);
            return memoizedFunc;
        }

        [PexGenericArguments(typeof(object), typeof(int))]
        [PexMethod]
        public void MemoizedFunction_WhenCalledSeveralTimesWithSameInput_ReturnsSameValue<T,TResult>(
            Func<T, TResult> f,
            T inputParameter)
        {
            var memoizedFunc = f.Memoize();
            var firstCallResult = memoizedFunc(inputParameter);
            var secondCallResult = memoizedFunc(inputParameter);

            PexObserve.Value("firstCallResult", firstCallResult);
            PexObserve.Value("secondCallResult", secondCallResult);
            PexAssert.AreEqual(firstCallResult, secondCallResult);
        }
    }
}
