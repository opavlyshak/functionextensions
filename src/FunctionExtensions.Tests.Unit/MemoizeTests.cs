﻿using System;
using NUnit.Framework;
using FluentAssertions;

namespace FunctionExtensions.Tests.Unit
{
    [TestFixture]
    public class MemoizeTests
    {
        [Test]
        public void Memoize_WhenCalled_ReturnsFunctionThatCachesResults()
        {
            int callsCount = 0;
            Func<double, int> func = d => ++callsCount;

            var memoizedFunc = func.Memoize();
            var firstCallResult = memoizedFunc(1);
            var secondCallResult = memoizedFunc(1);

            callsCount.Should().Be(1);
            firstCallResult.Should().Be(1);
            secondCallResult.Should().Be(1);
        }

        [Test]
        public void Memoize_WhenCalled_DoesNotEvaluateFunctionUntilResultingFunctionIsCalled()
        {
            int callsCount = 0;
            Func<double, int> func = d => ++callsCount;

#pragma warning disable 168
            var memoizedFunc = func.Memoize();
#pragma warning restore 168

            callsCount.Should().Be(0);
        }

        [Test]
        public void MemoizedFunction_WhenCalledWithDefaultValueTypeValue_CachesResults()
        {
            int callsCount = 0;
            Func<double, int> func = d => ++callsCount;

            var memoizedFunc = func.Memoize();
            memoizedFunc(default(double));
            memoizedFunc(default(double));

            callsCount.Should().Be(1);
        }

        [Test]
        public void MemoizedFunction_WhenCalledWithReferenceType_CachesResult()
        {
            int callsCount = 0;
            Func<object, int> func = o => ++callsCount;
            var parameter = new object();

            var memoizedFunc = func.Memoize();
            memoizedFunc(parameter);
            memoizedFunc(parameter);

            callsCount.Should().Be(1);
        }

        [Test]
        public void MemoizedFunction_WhenCalledWithDifferentInputs_ReturnsDifferentValues()
        {
            int callsCount = 0;
            Func<object, int> func = o => ++callsCount;

            var memoizedFunc = func.Memoize();
            var firstCallResult = memoizedFunc(new object());
            var secondCallResult = memoizedFunc(new object());

            callsCount.Should().Be(2);
            firstCallResult.Should().NotBe(secondCallResult);
        }

        [Test]
        public void MemoizedFunction_WhenCalledWithNull_CachesResult()
        {
            int callsCount = 0;
            Func<object, int> func = o => ++callsCount;

            var memoizedFunc = func.Memoize();
            var firstCallResult = memoizedFunc(null);
            var secondCallResult = memoizedFunc(null);

            callsCount.Should().Be(1);
            firstCallResult.Should().Be(1);
            secondCallResult.Should().Be(1);
        }
    }
}